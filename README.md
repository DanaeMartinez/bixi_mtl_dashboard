## Bixi Montreal Dashboard

The data used in this dashboard corresponds to the Montreal [BIXI Bike sharing company](https://bixi.com/en). 
The BIXI bike data system records every ride's start and end location, time of day and duration. We have information about every time a bike was ridden in 2019.

![view](bixi_map.png) 

The data can be filtered by type of client (member or occasional), date range (from 14 April 2019 to 31 October 2019), hour of the day the bike was ridden and the trip’s duration. 

The app carries out several exploratory visualizations and descriptive statistics providing insights about the trips, such as the total number of trips, median trip’s duration, popular start and ending points, etc. 

![view](bixi_dash.png) 

The dashboard is available in this [link](https://danaemirelmartinez.shinyapps.io/Bike_Shares_Mtl_Bixi/).
